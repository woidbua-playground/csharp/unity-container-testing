﻿using System;

namespace UnityContainerTesting.Services
{
    public class Logger : ILogger
    {
        public void Log(string content)
        {
            Console.WriteLine(content);
        }
    }
}
