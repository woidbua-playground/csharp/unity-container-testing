﻿namespace UnityContainerTesting.Services
{
    public interface ILogger
    {
        void Log(string content);
    }
}
