﻿using System;
using Unity;
using Unity.Resolution;
using UnityContainerTesting.Models;
using UnityContainerTesting.Services;

namespace UnityContainerTesting
{
    public static class Program
    {
        private static void Main()
        {
            var container = GenerateContainer().AddExtension(new Diagnostic());

            var articleList = container.Resolve<IArticleList>();
            articleList.AddArticle();
            articleList.AddArticle();
            articleList.AddArticle();

            foreach (var article in articleList.Articles)
                Console.WriteLine(article.Name);
        }


        private static IUnityContainer GenerateContainer()
        {
            var container = new UnityContainer();

            container.RegisterType<ILogger, Logger>(TypeLifetime.PerContainer);
            container.RegisterType<IArticleList, ArticleList>();

            container.RegisterType<IArticle, Article>();
            container.RegisterFactory<Func<string, IArticle>>(
                c => new Func<string, IArticle>(name => c.Resolve<IArticle>(new ParameterOverride("name", name))),
                FactoryLifetime.PerContainer
            );
            container.RegisterType<IArticleFactory, ArticleFactory>(TypeLifetime.PerContainer);

            return container;
        }
    }
}
