﻿namespace UnityContainerTesting.Models
{
    public interface IArticleFactory
    {
        IArticle Create(string name);
    }
}
