﻿using UnityContainerTesting.Services;

namespace UnityContainerTesting.Models
{
    public class Article : Entity, IArticle
    {
        public Article(ILogger logger, string name)
            : base(logger)
        {
            Name = name;
        }


        public string Name { get; }
    }
}
