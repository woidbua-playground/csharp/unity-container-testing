﻿using System.Collections.Generic;

namespace UnityContainerTesting.Models
{
    public class ArticleList : IArticleList
    {
        private readonly IArticleFactory _articleFactory;
        private int _counter = 1;


        public ArticleList(IArticleFactory articleFactory)
        {
            _articleFactory = articleFactory;
        }


        public IList<IArticle> Articles { get; } = new List<IArticle>();


        public void AddArticle()
        {
            Articles.Add(_articleFactory.Create("article" + _counter++));
        }
    }
}
