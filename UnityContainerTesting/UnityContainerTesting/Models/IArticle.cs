﻿namespace UnityContainerTesting.Models
{
    public interface IArticle
    {
        string Name { get; }
    }
}
