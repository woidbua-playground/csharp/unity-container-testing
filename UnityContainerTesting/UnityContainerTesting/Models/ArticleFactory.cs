﻿using System;

namespace UnityContainerTesting.Models
{
    public class ArticleFactory : IArticleFactory
    {
        private readonly Func<string, IArticle> _articleFunc;


        public ArticleFactory(Func<string, IArticle> articleFunc)
        {
            _articleFunc = articleFunc;
        }


        public IArticle Create(string name)
        {
            return _articleFunc(name);
        }
    }
}
