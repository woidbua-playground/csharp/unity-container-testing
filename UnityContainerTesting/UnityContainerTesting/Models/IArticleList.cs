﻿using System.Collections.Generic;

namespace UnityContainerTesting.Models
{
    public interface IArticleList
    {
        IList<IArticle> Articles { get; }

        void AddArticle();
    }
}
